provider "aws" {
    shared_credentials_files = ["/Users/garthjackson/Downloads/credentials"]
    profile = "Academy"
    region = "eu-west-2"
}

data "terraform_remote_state" "my-infrastructure" {
  backend = "s3" 
  config = {
    bucket = "garth-bucket-academy-tf"  # For Academy account
    key    = "infra.tfstate-academy"
    region = "eu-west-2"
    shared_credentials_file = "/Users/garthjackson/Downloads/credentials"
    profile = "Academy"
  }
}
