variable "my_tag" {
  description = "Name to tag all elements with"
  default     = "garth"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.2.0/25"
}

variable "public_subnet_cidr" {
    description = "CIDR for the public subnet"
    default = "10.0.2.0/27"    
}

variable "public_subnet_cidr_2" {
    description = "CIDR for the public subnet 2"
    default = "10.0.2.96/27"    
}

variable "aws_region" {
    description = "AWS region"
    default = "eu-west-2"
}